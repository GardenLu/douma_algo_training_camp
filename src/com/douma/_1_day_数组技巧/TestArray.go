
// 计算输入数组中每个元素出现的次数
func countArray1(arr []int) map[int]int {
    var countMap = make(map[int]int)
    for _, num := range arr {
        if cnt, ok := countMap[num]; ok {
            countMap[num] = cnt + 1
        } else {
            countMap[num] = 1
        }
    }
    return countMap
}

// 数组每个元素的大小：1 <= arr[i] <= 6
func countArray2(arr []int) [6]int {
    var countMap [6] int
    for _, num := range arr {
        index := num - 1
        countMap[index]++
    }
    return countMap
}

func main() {
    var arr = []int{1, 1, 2, 1, 4}
    fmt.Println(countArray2(arr))
}