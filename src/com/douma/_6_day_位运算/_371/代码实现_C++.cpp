public:
    int getSum(int a, int b) {
        while (b != 0) {
            // 这里之所以 unsigned int 是因为：
            // LeetCode 自己的编译器比较 strict，不能对负数进行左移，就是说最高位符号位必须要为 0，才能左移
            int carry = ((unsigned int)(a & b)) << 1;
            a = a ^ b;
            b = carry;
        }
        return a;
    }