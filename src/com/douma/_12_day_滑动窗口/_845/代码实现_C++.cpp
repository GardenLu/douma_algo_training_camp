class Solution {
public:
    int longestMountain(vector<int>& arr) {
        int n = arr.size();
        int ans = 0;
        int left = 0;
        while (left + 2 < n) {
            int right = left + 1;
            if (arr[left] < arr[right]) {
                while (right + 1 < n && arr[right] < arr[right + 1]) right++;
                if (right + 1 < n && arr[right] > arr[right + 1]) {
                    while (right + 1 < n && arr[right] > arr[right + 1]) right++;
                    ans = max(ans, right - left + 1);
                } else {
                    // 注意，这个分支不会执行到，可以去掉
                    // 因为 arr[right] == arr[right + 1] 的话，都不会走到 if (arr[left] < arr[right]) { 这个分支的
                    right++;
                }
            }
            left = right;
        }
        return ans;
    }
};