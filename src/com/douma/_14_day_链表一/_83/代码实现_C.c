/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* deleteDuplicates(struct ListNode* head) {
    if (head == NULL || head->next == NULL) return head;

    struct ListNode* prev = head;
    struct ListNode* curr = head->next;
    while (curr != NULL) {
        if (curr->val == prev->val) {
            prev->next = curr->next;
            curr->next = NULL;
            free(curr);  // 释放重复结点的内存
        } else {
            prev = curr;
        }
        curr = prev->next;
    }
    return head;
}