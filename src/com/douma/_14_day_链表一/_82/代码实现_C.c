/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* deleteDuplicates(struct ListNode* head) {
    if (head == NULL || head->next == NULL) return head;

    struct ListNode* dummyNode = malloc(sizeof(struct ListNode));
    dummyNode->val = -1;
    dummyNode->next = head;

    struct ListNode* prev = dummyNode;
    struct ListNode* curr = head;
    while (curr != NULL) {
        if (curr->next != NULL && curr->val == curr->next->val) {
            do {
                curr = curr->next;
            } while (curr->next != NULL && curr->val == curr->next->val);
            prev->next = curr->next;
            curr->next = NULL;
            free(curr);  // 释放重复结点的内存
        } else {
            prev = curr;
        }
        curr = prev->next;
    }
    head = dummyNode->next;
    free(dummyNode);
    return head;
}